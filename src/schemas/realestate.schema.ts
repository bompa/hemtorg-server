import { Schema } from "mongoose";
import { RealEstateStore } from "../interfaces/realestatestore.interface";

export var realEstateSchema: Schema = new Schema({
  userID: String,
  createdAt: Date,

  address: String,
  postcode: String,
  type: Number,
  size: String,
  priceRange: {
    min: Number,
    max: Number
  },
  monthlyFee: Number,
  city: String,
  owner: String,
  placeID: String,
  loc: {
    lat: Number,
    lng: Number
  },
  images: [],
  isPrivate: Boolean,
});

realEstateSchema.pre("save", function (next) {
  if (!this.createdAt) {
    this.createdAt = new Date();
  }
  next();
});