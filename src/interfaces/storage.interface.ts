export interface Storage<T> {
  write(store: T): void;
  read(id: string): T;
  list(query, callback): void;
}