
export interface BlobStore {
  data: Buffer;
  contentType: string;
};
