import { Storage } from './storage.interface'

export interface StorageAccess {
  init(): void;

  getStorageObject<T>(): Storage<T>;
}