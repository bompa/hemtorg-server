import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as logger from "morgan";
import * as path from "path";
import * as cors from "cors";
import errorHandler = require("errorhandler");
import methodOverride = require("method-override");
import { MongooseDatabase } from "./storage/mongoosedb";
import { MemoryDatabase } from "./storage/memorystorageaccess";
import { StorageAccess } from "./interfaces/storageaccess.interface";

import { BaseRoute } from "./routes/baseroute";
import { IndexRoute } from "./routes/index";
import { RealEstateRoute } from "./routes/realestates";
import { RealEstateStore } from "./interfaces/realestatestore.interface"; //import IUser

const winston = require('winston');

var jwt = require('express-jwt');
var auth0Settings = require('./conf/auth0.json');
var corsAllowedOrigins = require('./conf/allowed_origins.json');
var dbConf = require('../conf/database.json');

function anonymousRequest(req) {
  let userID = req.header('x-userid');
  if (userID) {
    req.userID = userID;
    return false
  }
  return true;
}

export class Server {

  public app: express.Application;
  private jwt: any;
  private createdRoutes: BaseRoute[] = [];

  /**
   * Bootstrap the application.
   *
   * @class Server
   * @method bootstrap
   * @static
   * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
   */
  public static bootstrap(storage: StorageAccess): Server {
    return new Server(storage);
  }

  constructor(private storage: StorageAccess) {
    this.app = express();

    //configure application
    this.config();
    this.routes();
  }

  /**
   * Create REST API routes
   *
   * @class Server
   * @method api
   */
  public routes() {
    let router: express.Router;
    router = express.Router();

    this.createdRoutes.push(IndexRoute.create(router));
    this.createdRoutes.push(RealEstateRoute.create(router,
      this.storage.getStorageObject<RealEstateStore>()));

    //use router middleware
    this.app.use(router);
  }

  /**
   * Configure application
   *
   * @class Server
   * @method config
   */
  public config() {
    var corsOptions = {
      origin: corsAllowedOrigins,
      allowedHeaders: ['Content-Type', 'Authorization', 'X-UserID'],
      credentials: true,
      optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
    }
    this.app.use(cors(corsOptions));
    this.app.use(bodyParser.json({ limit: '50mb' }));

    var jwtCheck = jwt({
      secret: auth0Settings.secret,
      audience: auth0Settings.audience
    });

    let unless = function (path, method, middleware) {
      return function (req, res, next) {
        req.anonymous = anonymousRequest(req);
        if (req.anonymous && (path === req.path) && (method === req.method)) {
          return next();
        } else {
          return middleware(req, res, next);
        }
      };
    }

    this.app.use(unless(RealEstateRoute.PATH_NAME, 'GET', jwtCheck));
    this.app.use(logger("dev"));
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({
      extended: true
    }));

    this.connectDB();

    //catch 404 and forward to error handler
    this.app.use(function (err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
      err.status = 404;
      next(err);
    });

    //error handling
    this.app.use(errorHandler());
  }

  private connectDB() {
    this.storage.init();
  }
}

var g_usedPort = 0;

export function init() {
  var debug = require("debug")("express:server");
  var http = require("http");

  winston.level = process.env.LOG_LEVEL || 'verbose';
  //winston.add(winston.transports.File, { filename: 'somefile.log' });
  /*winston.configure({
    transports: [
      new (winston.transports.Console)(),
      new (winston.transports.File)({ filename: 'ht_server.log' })
    ]
  });*/

  //create http server
  var httpPort = normalizePort(process.env.PORT || 4001);
  g_usedPort = httpPort;
  
  let dbType: string = dbConf.useMemoryDB? 'memory': 'mongodb';
  console.log('Startup using ' + dbType  + ' database')
  
  let db = dbConf.useMemoryDB? new MemoryDatabase(): new MongooseDatabase();
  var app = Server.bootstrap(db).app;
  
  app.set("port", httpPort);
  var httpServer = http.createServer(app);

  //listen on provided ports
  httpServer.listen(httpPort);

  //add error handler
  httpServer.on("error", onError);

  console.log('Server listening on port 4001.')
  //start listening on port
  httpServer.on("listening", onListening);


  /**
   * Normalize a port into a number, string, or false.
   */
  function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
      // named pipe
      return val;
    }

    if (port >= 0) {
      // port number
      return port;
    }

    return false;
  }

  /**
   * Event listener for HTTP server "error" event.
   */
  function onError(error) {
    if (error.syscall !== "listen") {
      throw error;
    }

    var bind = typeof g_usedPort === "string"
      ? "Pipe " + g_usedPort
      : "Port " + g_usedPort;

    // handle specific listen errors with friendly messages
    switch (error.code) {
      case "EACCES":
        console.error(bind + " requires elevated privileges");
        process.exit(1);
        break;
      case "EADDRINUSE":
        console.error(bind + " is already in use");
        process.exit(1);
        break;
      default:
        throw error;
    }
  }

  /**
   * Event listener for HTTP server "listening" event.
   */
  function onListening() {
    var addr = httpServer.address();
    var bind = typeof addr === "string"
      ? "pipe " + addr
      : "port " + addr.port;
    debug("Listening on " + bind);
  }
}