import { suite, test, slow, timeout, skip, only } from "mocha-typescript";
import { RealEstateStore } from "../interfaces/realestatestore.interface";
import { RealEstateModel } from "../models/realestate.model";
import { realEstateSchema } from "../schemas/realestate.schema";
import mongoose = require("mongoose");

var dbConf = require('../conf/database.json');

@suite
class RealEstateTest {
  //store test data
  private data: RealEstateStore;

  //the User model
  public static RealEstateData: mongoose.Model<RealEstateModel>;

  public static before() {
    //use q promises
    global.Promise = require("q").Promise;

    //use q library for mongoose promise
    mongoose.Promise = global.Promise;

    //connect to mongoose and create model
    const MONGODB_CONNECTION: string = dbConf.dbConnectionString;
    let connection: mongoose.Connection = mongoose.createConnection(MONGODB_CONNECTION);
    RealEstateTest.RealEstateData = connection.model<RealEstateModel>("RealEstate", realEstateSchema);

    //require chai and use should() assertions
    let chai = require("chai");
    chai.should();
  }

  constructor() {
    this.data = {
      address: "Ostidiefararen 17",
      postcode: "41765",
      city: "Göteborg",
      owner: "Jonas",
      type: 0,
      size: "100",
      priceRange: { min: 1, max: 100 },
      monthlyFee: 0,
      loc: { lat: 1, lng: 2 },
      isPrivate: false
    };
  }

  @test("should create a new RealEstate")
  public create() {
    return new RealEstateTest.RealEstateData(this.data).save().then(result => {
      //verify _id property exists
      result._id.should.exist;

      result.owner.should.equal(this.data.owner);
      result.city.should.equal(this.data.city);
      result.postcode.should.equal(this.data.postcode);
    });
  }

}