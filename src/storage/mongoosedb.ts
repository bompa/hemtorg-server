import { StorageAccess } from '../interfaces/storageaccess.interface';
import { Storage } from '../interfaces/storage.interface';
import { RealEstateStore } from '../interfaces/realestatestore.interface';
import { MongooseRealEstateStorage } from './mongooserealestatestorge';
import mongoose = require("mongoose");
import { Model } from "mongoose";
import { StoreModel } from "../models/model";
import { RealEstateModel } from "../models/realestate.model";
import { realEstateSchema } from "../schemas/realestate.schema";

var dbConf = require('../conf/database.json');

export class MongooseDatabase implements StorageAccess {

  private model: StoreModel = Object();

  constructor() {

  }

  init(): void {
    global.Promise = require("q").Promise;
    mongoose.Promise = global.Promise;

    if (!dbConf.dbConnectionString) {
      console.error('Missing db conneciton stirng, aborting..');
      throw ("Error: missing database connection, expected 'dbConnectionString'.");
    }
    //connect to mongoose
    let connection: mongoose.Connection = mongoose.createConnection(dbConf.dbConnectionString);
    if(dbConf.dbName) {
      connection = connection.useDb(dbConf.dbName);
    }
    this.model.realEstates = connection.model<RealEstateModel>("RealEstate", realEstateSchema);

  }

  getStorageObject<MongooseRealEstateStorage>(): Storage<RealEstateStore> {
    return MongooseRealEstateStorage.create(this.model.realEstates);
  }
}
