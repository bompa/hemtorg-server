import { Storage } from '../interfaces/storage.interface'
import { RealEstateStore } from '../interfaces/realestatestore.interface'
import { RealEstateModel } from "../models/realestate.model";
import { Model } from "mongoose";
import mongoose = require("mongoose");

const winston = require('winston');

export class MongooseRealEstateStorage implements Storage<RealEstateStore> {

  static create(model: mongoose.Model<RealEstateModel>): Storage<RealEstateStore> {
    return new MongooseRealEstateStorage(model);
  }

  constructor(private model: mongoose.Model<RealEstateModel>) {

  }

  write(store: RealEstateStore): void {
    new this.model(store).save()
      .then(storedEstate => {
        winston.debug('Saved document with city value ' + storedEstate.city);
      })
      .catch(error => this.handleError(error, store));
  }

  handleError(error, store) {
    winston.error('Failed to save realEstate object ' + store)
  }

  read(id: string): RealEstateStore {
    return {
      address: "",
      postcode: "",
      type: -1,
      size: "",
      priceRange: { min: 0, max: 0 },
      monthlyFee: 0,
      city: "",
      owner: "",
      placeID: "",
      loc: { lng: 0, lat: 0 },
      isPrivate: true
    };
  }

  list(query, callback) {
    this.model.find(query.filter, (err, estates) => {
      if(estates) {
        winston.debug('Found ' + estates.length + ' estates.')
      }     
      callback(err, estates);
    })
      .catch(error => winston.error('Failed to list realEstates collection'));
  }
}