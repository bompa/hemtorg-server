import { Storage } from '../interfaces/storage.interface'
import { RealEstateStore } from '../interfaces/realestatestore.interface'
import { RealEstateModel } from "../models/realestate.model";
import { Model } from "mongoose";
import mongoose = require("mongoose");

const winston = require('winston');

export class MemoryRealEstateStorage implements Storage<RealEstateStore> {

  estates: RealEstateStore[] = [];

  static create() {
    return new MemoryRealEstateStorage();
  }

  constructor() {

  }

  write(store: RealEstateStore): void {
    this.estates.push(store);
  }

  read(id: string): RealEstateStore {
    return this.estates[0];
  }

  list(query, callback) {
    callback(null, this.estates);
  }
}