import { StorageAccess } from '../interfaces/storageaccess.interface';
import { Storage } from '../interfaces/storage.interface';
import { RealEstateStore } from '../interfaces/realestatestore.interface';
import { RealEstateModel } from "../models/realestate.model";
import { realEstateSchema } from "../schemas/realestate.schema";
import { MemoryRealEstateStorage } from "./memoryrealestatestorge";

export class MemoryDatabase implements StorageAccess {

  constructor() {

  }

  init(): void {
  }

  getStorageObject<MemoryRealEstateStorage>(): Storage<RealEstateStore> {
    return MemoryRealEstateStorage.create();
  }
}
