import { NextFunction, Request, Response } from "express";

/**
 * Constructor
 *
 * @class BaseRoute
 */
export class BaseRoute {

  protected title: string;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor() {
    //initialize variables
    this.title = "HemTorg REST Endpoint";
  }

  /**
   * Prepare a resonse.
   *
   * @class BaseRoute
   * @method render
   * @param req {Request} The request object.
   * @param res {Response} The response object.
   * @return void
   */
  public prepare(req: Request, res: Response) {
    //add title
    res.locals.title = this.title;
  }
}