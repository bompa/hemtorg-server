import { NextFunction, Request, Response, Router } from "express";
import { BaseRoute } from "./baseroute";

const winston = require('winston');


/**
 * / route
 *
 * @class User
 */
export class IndexRoute extends BaseRoute {

  /**
   * Create the routes.
   *
   * @class IndexRoute
   * @method create
   * @static
   */
  public static create(router: Router) {
    return new IndexRoute(router);
  }

  /**
   * Constructor
   *
   * @class IndexRoute
   * @constructor
   */
  constructor(router: Router) {
    super();
    this.init(router);
  }

  private init(router: Router) {
    //log
    console.log("[IndexRoute::create] Creating index route.");

    //add home page route
    router.get("/", (req: Request, res: Response, next: NextFunction) => {
      this.index(req, res, next);
    });
  }

  /**
   * The home page route.
   *
   * @class IndexRoute
   * @method index
   * @param req {Request} The express Request object.
   * @param res {Response} The express Response object.
   * @next {NextFunction} Execute the next method.
   */
  public index(req: Request, res: Response, next: NextFunction) {
    //set custom title
    this.title = "Home | " + this.title;

    //prepare response
    this.prepare(req, res);

    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end('This is the Hemtorg backend server, hosting the Hemtorg REST API.');
  }
}