import { NextFunction, Request, Response, Router } from 'express';
import { BaseRoute } from './baseroute';
import { Storage } from '../interfaces/storage.interface';
import { RealEstateStore } from '../interfaces/realestatestore.interface';
import { Query } from 'ht-public/queries/query';
import { RealEstatesQuery } from 'ht-public/queries/realestatesquery';

const winston = require('winston');

/**
 * /realestates route
 *
 * @class User
 */
export class RealEstateRoute extends BaseRoute {
  public static PATH_NAME: string = '/realestates';

  private q: RealEstatesQuery = RealEstatesQuery.create();

  public static create(router: Router, storage: Storage<RealEstateStore>) {
    return new RealEstateRoute(router, storage)
  }

  private getAnonymous(req: any) {
    return req.anonymous;
  }

  private getUser(req: any) {
    return req.user;
  }

  private getUserID(user: any) {
    let sub: string = user.sub;
    if (user && sub) {
      let idSplit: string[] = sub.split('|');
      if(idSplit && (idSplit.length > 1)) {
        return idSplit[1];
      }
    }
    return '';
  }

  private init(router: Router) {
    winston.debug("[RealEstateRoute::create] Creating realeaste route.");
    var self = this;

    router.get(RealEstateRoute.PATH_NAME, function (req, res) {
      let anonymous = self.getAnonymous(req);
      let user = self.getUser(req);
      if (anonymous) {
        let query = { filter: { isPrivate: false } };
        self.storage.list(query, (err, estates) => {
          res.json(estates);
        });
      }
      else if (user) {
        let userIdParam = req.query[self.q.params.filter.userID];
        let listQuery = {};
        if (userIdParam === Query.CURRENT_USER_VALUE) {
          listQuery = { filter: { userID: self.getUserID(user) } };
        }
        self.storage.list(listQuery, (err, estates) => {
          res.json(estates);
        });
      }
      else {
        winston.error('Missing authentication information for GET ' + RealEstateRoute.PATH_NAME);
      }
    });

    router.post(RealEstateRoute.PATH_NAME, function (req, res) {
      let user = self.getUser(req);
      if(user) {
        req.body.userID = self.getUserID(user);
        self.storage.write(req.body);
        res.send(200);
      }
      else {
        res.send(400, 'Missing user information');
      }
    })
  }

  constructor(router: Router,
    private storage: Storage<RealEstateStore>) {
    super();
    this.init(router);
  }
}