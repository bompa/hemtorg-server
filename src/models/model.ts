import { Model } from "mongoose";
import { RealEstateModel } from "./realestate.model";

export interface StoreModel {
  realEstates: Model<RealEstateModel>;
}