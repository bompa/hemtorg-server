import { Document } from "mongoose";
import { RealEstateStore } from "../interfaces/realestatestore.interface";

export interface RealEstateModel extends RealEstateStore, Document {
  //custom methods for your model would be defined here
}